#!/usr/bin/python

import serial
import time
from xbee import XBee, ZigBee
from digimesh import DigiMesh


serial_port = serial.Serial('/dev/ttyUSB0', 9600)

#xbee = XBee(serial_port)
xbee = DigiMesh(serial_port)
counter = 0

while True:
    try:
        time.sleep(2)

        xbee.send("at", frame='A', command='N' + str(counter%10))
        counter += 1
    except KeyboardInterrupt:
        break

xbee.halt()
serial_port.close()